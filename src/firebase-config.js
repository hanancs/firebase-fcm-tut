/**
 * To find your Firebase config object:
 * 
 * 1. Go to your [Project settings in the Firebase console](https://console.firebase.google.com/project/_/settings/general/)
 * 2. In the "Your apps" card, select the nickname of the app for which you need a config object.
 * 3. Select Config from the Firebase SDK snippet pane.
 * 4. Copy the config object snippet, then add it here.
 */
const config = {
  /* TODO: ADD YOUR FIREBASE CONFIGURATION OBJECT HERE */
  apiKey: "AIzaSyAgHQgahxd1mZpjfYbLTPgBE89BUlmC68Y",
  authDomain: "friendlychat-5d97c.firebaseapp.com",
  projectId: "friendlychat-5d97c",
  storageBucket: "friendlychat-5d97c.appspot.com",
  messagingSenderId: "7343906409",
  appId: "1:7343906409:web:a711c3b493b02d48720cf8"
};

export function getFirebaseConfig() {
  if (!config || !config.apiKey) {
    throw new Error('No Firebase configuration object provided.' + '\n' +
    'Add your web app\'s configuration object to firebase-config.js');
  } else {
    return config;
  }
}